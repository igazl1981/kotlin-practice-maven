package com.practice.easy

object Pangram {

    fun isPangram(text: String): Boolean {
        var letter: Char = "a".toCharArray()[0]
        val lower = text.toLowerCase()

        do {
            println(letter)
            if (letter !in lower) return false
            letter++
        } while (letter <= "z".toCharArray()[0])
        return true
    }
}

object Pangram2 {

    fun isPangram(text: String): Boolean {
        return text.toLowerCase()
            .toSet()
            .filter { c -> c in 'a'..'z' }
            .size == 26
    }
}

class Pangram3 {
    companion object {

        // Fastest solution (64% time of 2nd solution below)
        fun isPangram(str: String): Boolean {
            var numLetters = 26
            val letters = HashSet<Char>(numLetters)
            for (c in str.filter(Char::isLetter))
                if (letters.add(c.toLowerCase()))
                    numLetters--
            return numLetters == 0
        }

        // asSequence could be used for big data sets (not mentioned in the exercise), but
        // at the expense of inlined lambdas
        fun isPangram1(str: String) =
            str.asIterable().filter(Char::isLetter).distinctBy(Char::toLowerCase).count() == 26

        // Slowest solution
        fun isPangram2(str: String) =
            str.asSequence().filter(Char::isLetter).distinctBy(Char::toLowerCase).count() == 26
    }
}

object Pangram4 {

    private const val ALPHABET = "abcdefghijklmnopqrstuvwxyz"

    fun isPangram(s: String): Boolean {
        val usedLetters: Set<Char> = s.toLowerCase()
            .filter { ALPHABET.contains(it) }
            .toSet()
        return usedLetters.size == ALPHABET.length
    }
}
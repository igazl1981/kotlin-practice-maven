package com.practice.easy

import java.time.LocalDate
import java.time.LocalDateTime
import java.time.LocalTime

class Gigasecond {
    val date: LocalDateTime
    val giga: Long = 1_000_000_000

    constructor(moment: LocalDateTime) {
        date = moment.plusSeconds(giga)
    }

    constructor(moment: LocalDate) : this(moment.atStartOfDay())
}

class Gigasecond2(origin: LocalDateTime) {
    val date = origin.plusSeconds(1_000_000_000)
    /** blabla */

    /** Alternate constructor
     * @param origin original date, time is assumed as being 00:00:00
     */
    constructor(origin: LocalDate) : this(LocalDateTime.of(origin, LocalTime.of(0, 0, 0)))
}

const val GIGA = 1_000_000_000L

class Gigasecond3(initialDateTime: LocalDateTime) {

    constructor(initialDate: LocalDate) : this(initialDate.atStartOfDay())

    val date: LocalDateTime = initialDateTime.plusSeconds(GIGA)

}

class Gigasecond4(dateTime: LocalDateTime) {
    constructor(date: LocalDate) : this(date.atStartOfDay())

    var date = dateTime
        get() = field.plusSeconds(1_000_000_000L)
}
package com.practice.easy

fun transcribeToRna(dna: String): String = dna.split("").map({ symbol: String ->
    when (symbol) {
        "G" -> "C"
        "C" -> "G"
        "T" -> "A"
        "A" -> "U"
        else -> ""
    }
}).joinToString("")

fun transcribeToRna2(dna: String) = dna.split("").map { symbol: String ->
    when (symbol) {
        "G" -> "C"
        "C" -> "G"
        "T" -> "A"
        "A" -> "U"
        else -> ""
    }
}.joinToString("")

fun transcribeToRna3(dna: String) = dna.split("").joinToString("") { symbol: String ->
    when (symbol) {
        "G" -> "C"
        "C" -> "G"
        "T" -> "A"
        "A" -> "U"
        else -> ""
    }
}
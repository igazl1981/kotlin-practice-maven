package com.practice.easy

import java.util.regex.Pattern

class Acronym {
    companion object {
        fun generate(input: String): String {
            return input.split(" ", "-")
                .filter { it.isNotBlank() }
                .joinToString("") { it.first().toString().toUpperCase() }
        }
    }
}

object Acronym2 {

    fun generate(s: String): String {
        return s.split(Pattern.compile("\\s|-")).filter { s -> s.isNotEmpty() }.map { s -> s.get(0).toUpperCase() }
            .joinToString("")
    }

    fun generate2(text: String) =
        text.split(Pattern.compile("\\s|-")).filter(String::isNotEmpty).map { s -> s[0].toUpperCase() }.joinToString("")

    fun generate4(text: String) =
        text.split(Pattern.compile("\\s|-")).filter(String::isNotEmpty).map { it[0].toUpperCase() }.joinToString("")
}

class Acronym3 {

    companion object {
        fun generate(phrase: String): String = phrase.split(" ", "-")
            .mapNotNull { word -> word.firstOrNull() }
            .joinToString("")
            .toUpperCase()
    }
}
package com.practice.easy

fun reverse(text: String) = text.reversed()
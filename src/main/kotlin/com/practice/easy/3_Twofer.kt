package com.practice.easy

fun twofer(other: String = "you") = "One for ${other}, one for me."
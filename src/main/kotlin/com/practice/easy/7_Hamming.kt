package com.practice.easy

object Hamming {
    fun compute(strandA: String, strandB: String): Number {
        require(strandA.length == strandB.length) { "left and right strands must be of equal length." }
        return strandA.zip(strandB).count { it.first != it.second }
    }

    fun compute1(strand1: String, strand2: String): Int {
        if (strand1.length != strand2.length)
            throw IllegalArgumentException("left and right strands must be of equal length.")
        //return strand1.zip(strand2).filter { it.first != it.second }.count()
        //return strand1.zip(strand2) { c1, c2 -> c1 to c2 }.filter { it.first != it.second }.count()
        return strand1.zip(strand2) { c1, c2 -> c1 != c2 }.count { it }
    }

    fun compute2(strand1: String, strand2: String): Int {
        if (strand1.length != strand2.length)
            throw IllegalArgumentException("left and right strands must be of equal length.")
        var delta = 0
        for ((idx, v) in strand1.withIndex())
            if (v != strand2[idx])
                delta++
        return delta
    }

    fun compute3(strand1: String, strand2: String): Int {
        if (strand1.length != strand2.length)
            throw IllegalArgumentException("left and right strands must be of equal length.")
        var delta = 0
        for (i in 0.until(strand1.length))
            if (strand1[i] != strand2[i])
                delta++
        return delta
    }

    fun compute4(strand1: String, strand2: String): Int {
        if (strand1.length != strand2.length)
            throw IllegalArgumentException("left and right strands must be of equal length.")
        var delta = 0
        val it1 = strand1.iterator()
        for (v in strand2)
            if (it1.next() != v)
                delta++
        return delta
    }
}


// Possible solution if the package is named Hamming and use a top level function
//
// package Hamming
//
//    public fun compute(strandA: String, strandB: String): Number {
//        require(strandA.length === strandB.length) { "left and right strands must be of equal length." }
//        return strandA.zip(strandB).count { it.first != it.second }
//    }
# 2. Reverse string

Print the given string reversed.

For example: input: `"cool"` output: `"looc"`